![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/1687344487_bogatyr-club-p-zelenii-les-oboi-vkontakte-23.jpg)

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
git init  
git add .  
git commit -m "Push existing project to GitLab"  
git remote add origin https://gitlab.com/Antoniii/linear-discriminant-analysis-with-python-scikit-learn.git
git branch -M main
git push -uf origin main
```

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/pca.PNG)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/Figure_0.png)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/Figure_19.png)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/lda.PNG)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/p58s2.png)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/%D0%9B%D0%B5%D1%81-%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D0%B9-660x371.png)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/pcadla.PNG)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/test-predict.PNG)  

![](https://gitlab.com/Pho_Bo_for_U/linear-discriminant-analysis-with-python-scikit-learn/-/raw/main/YBi3f1BdeLc.jpg)  

## Sources

* [Linear Discriminant Analysis with Python scikit-learn](https://wellsr.com/python/linear-discriminant-analysis-for-dimensionality-reduction-in-python/)
* [pima-indians-diabetes.csv](https://gist.github.com/dmpe/bfe07a29c7fc1e3a70d0522956d8e4a9)
* [Linear Discriminant Analysis (LDA) in Machine Learning](https://www.pythonprog.com/linear-discriminant-analysis-in-machine-learning/)
* [Linear Discriminant Analysis (LDA)](https://michael-fuchs-python.netlify.app/2020/08/07/linear-discriminant-analysis-lda/)
* [Алгоритм классификации Random Forest на Python](https://pythonru.com/uroki/sklearn-random-forest)
* [Customer Churn Prediction with Python](https://learnpython.com/blog/python-customer-churn-prediction/)
* [Can I save prediction value in same csv file as a another column using panda python](https://datascience.stackexchange.com/questions/45074/can-i-save-prediction-value-in-same-csv-file-as-a-another-column-using-panda-pyt)
* [Алгоритмы K-ближайших соседей и K-средних на Python](https://pythonru.com/uroki/sklearn-kmeans-i-knn)
